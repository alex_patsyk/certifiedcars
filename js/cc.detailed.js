function myMap() {
	var uluru = {lat: 40.423216, lng: -79.766558};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 4,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  map: map
	});
}



(function($){
	$(document).ready(function(){
		var readBtn       = $('.read-btn'),
			shareBtn 	      = $('.share-btn'), 
			likeBtn         = $('.like-btn'),
			viewPhoneBtn    = $('.view-phone'),
			networkOver     = $('.mobile-networks-over'),
			networkCloseBtn = $('.mobile-networks .close-btn'),
			navSliderHeight = $('.nav-main-photo-slider').outerHeight(),
			popupContainer  = $('.popups-container'),
			wrap            = $('#wrap');

		wrap.addClass('detail');

		// hide slider-nav on resize
		$(window).resize(function(){
			var windowWidth = $(window).width();

			navSliderHeight = $('.nav-main-photo-slider').outerHeight();
			if(windowWidth < 768) {
				$('.nav-main-photo-slider-wrap').css({bottom: -navSliderHeight}).addClass('closed');
				$('#trade-value-iframe').height($('.mobile-scroll').height() - 90);
			} else {
				$('#trade-value-iframe').height($('.car-photos').height() - 40);
			}
		});

		var windowWidth = $(window).width();
		if(windowWidth < 768) {
			$('.nav-main-photo-slider-wrap').css({bottom: -navSliderHeight}).addClass('closed');
			$('#trade-value-iframe').height($('.mobile-scroll').height() - 90);
		} else {
			$('#trade-value-iframe').height($('.car-photos').height() - 40);
		}
 
		$('.slider-action-btn').click(function(){
			var parent = $(this).closest('.nav-main-photo-slider-wrap');
			if(parent.hasClass('closed')) {
				parent.removeClass('closed');
				parent.animate({
					bottom: 0
				}, 300);
			} else {
				parent.addClass('closed');
				parent.animate({
					bottom: -navSliderHeight
				}, 300);
			}
		});

		function removeClass () {
			$('.popups-content-item, #buy-online, .buy-online-form, .buy-online-over').fadeOut();
			$('body').removeClass('overflow');
			$('.popup-btns .btn').removeClass('active');
			$('.dealer-details').css({'padding-top':0});
			hidePopupContainer ();
		}
		function hidePopupContainer () {
			popupContainer.animate({
				height: 'auto'
			},500, function(){
				popupContainer.removeClass('visible');
			});
		}

		//show/hide popups 
		$(".popup-btn").click(function(e){
			e.preventDefault();
			var hrefItem = $(this).attr('href'),
					windowWidth = $(window).width(),
					containerHeight = popupContainer.height() + $('header').height() + 25,
					windowHeight = window.innerHeight;
			if(windowWidth > 767) {
				$('html,body').animate({scrollTop:0}, 300).addClass('overflow');
				$('#trade-value-iframe').height($('.car-photos').height() - 31);
				if(containerHeight > windowHeight) {
					$('.row-overflow').addClass('fixed');
					$('.dealer-details').css({'padding-top': popupContainer.height()});
				}
				if($(this).hasClass('active')) {
					hidePopupContainer();
					$(this).removeClass('active'); 
					$(hrefItem).fadeOut();
					$('body').removeClass('overflow');
					$('.row-overflow').removeClass('fixed');
					$('.dealer-details').css({'padding-top':0});
					$('.buy-online-over').fadeOut();
				} else {
					$('.buy-online-over').fadeIn();
					popupContainer.addClass('visible'); 
					$('.popup-btns .btn').removeClass('active');
					$(this).addClass('active'); 
					$('.popups-content-item').fadeOut(0);
					$(hrefItem).delay(300).fadeIn(); 
				}
				
			} else {
				$('body').addClass('overflow');
				$('#trade-value-iframe').height(windowHeight - 90);
				popupContainer.addClass('visible'); 
				$(hrefItem).height(window.innerHeight).addClass('visible');
				$('.buy-online-over').fadeIn();
			}	
			if($(this).hasClass('exclusive-price-btn')) {
				$('.popups-wrap .popups-header').hide(0);
			}	else {
				$('.popups-wrap .popups-header').delay(300).fadeIn();
			}
			$('.with-arrow').removeClass('with-arrow');
		});
		

		$('.popups-close, .popups-header .car-photo, .popups-header h2, .buy-online-over').click(function(){
			var windowWidth = $(window).width();
			if(windowWidth > 767) {
				removeClass();
			} else {
				$('.popups-content-item').height('inherit').removeClass('visible');
				hidePopupContainer();
				$('.buy-online-over').fadeOut();
			}
			$('body').removeClass('overflow');
			$('.row-overflow').removeClass('fixed transparent');
		});

		$('.row-overflow').click(function(e) {
			if($(e.target).hasClass('transparent') || $(e.target).hasClass('row-overflow-scroll')) {
				$(this).removeClass('fixed transparent');
				removeClass();
			}
		});

		$('.popup-btns .btn-buy-online').click(function(e){
			e.preventDefault();
			var hrefItem = $(this).attr('href'),
					windowWidth = $(window).width(),
					containerHeight = popupContainer.height() + $('header').height() + 25,
					windowHeight = window.innerHeight;

			if(windowWidth > 767) {
				if(containerHeight > windowHeight) {
					$('.row-overflow').addClass('fixed transparent');
					$('.dealer-details').css({'padding-top': popupContainer.height()});
				}
				hidePopupContainer();
				$('.popup-btns .btn').removeClass('active');
				$('body').stop().animate({scrollTop:0}, 300).addClass('overflow');
				$(hrefItem).delay(300).fadeIn();
				$('.buy-online-form, .buy-online-over, .popups-wrap .popups-header').delay(300).fadeIn();
			}	
			$('.popup-btns .with-arrow').removeClass('with-arrow');	 
		});

		$('.btn-buy-online-iframe').click(function(e){
			e.preventDefault();
			$('#buy-online-iframe').animate({
				left: 0
			}, 500);
		});
		$('.close-frame').click(function(){
			$('#buy-online-iframe').animate({
				left: '100%'
			}, 500);
		});

		/// Parse Url function 
		function parseUrlQuery() {
		    var data = {};
		    if(location.search) {
		        var pair = (location.search.substr(1)).split('&');
		        for(var i = 0; i < pair.length; i ++) {
		            var param = pair[i].split('=');
		            data[param[0]] = param[1];
		        }
		    }
		    return data;
		}
		
		if(location.search) {
			var windowWidth = $(window).width(),
					hrefPopup = '#' + parseUrlQuery().popup,
					hrefBtn   = '.' + parseUrlQuery().btn;
			popupContainer.addClass('visible'); 
			if(windowWidth > 767) {
				$('.popup-btns .btn').removeClass('active');
				$(hrefBtn).addClass('active'); 
				$(hrefPopup).fadeIn();
			} else {
				$(hrefPopup).height(window.innerHeight).addClass('visible');
				$('.buy-online-over').fadeIn();
			}	
		}

		// affix effect for popup buttons block
		$('#affix').affix({
      offset: {
        top: 113,
        bottom: $("footer").outerHeight(true) + 400
      }
		});

		// sharing BUTTONS
		$("#desktop-sharing").jsSocials({
		    showCount: false,
		    showLabel: false,
		    shares: ["twitter", "facebook", "googleplus"]
		});
		$("#mobile-sharing").jsSocials({
		    showCount: false,
		    showLabel: true,
		    shares: ["email", "twitter", "facebook", "googleplus", "whatsapp"]
		});

		$('.other-cars-slider').slick({
 			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			responsive: [
        {
          breakpoint: 992,
          settings: {
              slidesToShow: 3
          }
        },
        {
          breakpoint: 767,
          settings: {
              slidesToShow: 2
          }
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1
          }
        }
      ]
		});

		function setSlideCount() {
		  var $el = $('.photo-count').find('.total');
		  $el.text(slideCount);
		}

		function setCurrentSlideNumber(currentSlide) {
		  var $el = $('.photo-count').find('.current');
		  $el.text(currentSlide + 1);
		}

		$('.main-photo-slider').on('init', function(event, slick){
		  slideCount = slick.slideCount;
		  setSlideCount();
		  setCurrentSlideNumber(slick.currentSlide);
		}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		  setCurrentSlideNumber(nextSlide);
		}).on('afterChange',function(e,o){
		    $('iframe').each(function(){
		        $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
		    });
		});

		$('.main-photo-slider').slick({
 			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: true,
			//asNavFor: '.nav-main-photo-slider'
		});

		

		$('.nav-main-photo-slider').slick({
			slidesToShow: 13,
			slidesToScroll: 1,
			arrows: true,
			focusOnSelect: true,
			speed: 700,
			variableWidth: true,
			autoplay: true,
			autoplaySpeed: 3000,
			centerMode: true,
			responsive: [
        {
          breakpoint: 1200,
          settings: {
              slidesToShow: 10
          }
        },
        {
          breakpoint: 992,
          settings: {
              slidesToShow: 8
          }
        },
        {
          breakpoint: 767,
          settings: {
              slidesToShow: 8,
              autoplay: false
          }
        }
      ]
		});

		 $(".nav-main-photo-slider-wrap .main-slide-item").click(function(e){
      e.preventDefault();
      var dataId = parseInt($(this).attr('data-id'));
      $( '.main-photo-slider' ).slick('slickGoTo', dataId);
    });

		$('#car-gallery').lightGallery({
			download:false,
			share:false,
			actualSize:false,
			zoom:false,
			autoplayControls:false,
			selector: '.main-slide-item'
		});

		// read full description
		readBtn.click(function(e){
			e.preventDefault();
			var parent = $(this).parent();
			if(parent.hasClass('more')) {
				parent.removeClass('more');
				$(this).html('SHOW MORE <span>+</span>');
				$(this).closest('.vehicle-overview').find('.mobile-hide').hide(500);
			} else {
				parent.addClass('more');
				$(this).html('SHOW LESS <span>-</span>');
				$(this).closest('.vehicle-overview').find('.mobile-hide').show(500);
			}
		});

		likeBtn.click(function(e){
			e.preventDefault();
			$(this).parent().toggleClass('liked');
		})

		shareBtn.click(function(e) {
			e.preventDefault();
			$('.mobile-networks').addClass('visible');
			$('.mobile-networks-over').addClass('visible');
		});
		networkOver.click(function(){
			$(this).removeClass('visible');
			$('.mobile-networks').removeClass('visible');
		});
		networkCloseBtn.on('click', function(){
			$(this).parent().removeClass('visible');
			$('.mobile-networks-over').removeClass('visible');
		});

		// Add class for colored select 
		$('.popups-content-item select').change(function(){
			if($(this).val() !== '-1') {
				$(this).closest('.select-wrap').addClass('selected');
			} else {
				$(this).closest('.select-wrap').removeClass('selected');
			}
		});

		// Checking on emplty/ show error message
		$('.popups-content-item .btn').click(function(){
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i,
					count = 0,
					countVal = 0;
			$(this).closest('.popups-content-item').find('input.required, select.required').each(function(){
				count ++;
				if($(this).val() == "" || $(this).val() == "-1") {
					$(this).addClass('error');
					$(this).closest('.select-wrap').addClass('error');
				} else {
					if(!(pattern.test($(this).attr('type','email').val()))){
		        $(this).addClass('error');
		        $('.error').selectpicker('refresh');
		      }
		      $(this).removeClass('error');
		      $(this).closest('.select-wrap').removeClass('error');
		      countVal ++;
				}
			});
			if(countVal < count) {
				$('#error-message').show();
				return false;
			}
		});

		$('.popups-content-item').on('change', '.error', function(){
			$(this).closest('.select-wrap').removeClass('error');
			$(this).removeClass('error');
			$('#error-message').hide();
		});


		// JS code for Finance range sliders
		$('.range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this),
				  value  = slideEvt.value;
			slider.closest('.range-slider-wrap').find('.slider-val').val(value);
			//getRangeSliderValue (slider, slideEvt.value);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this),
					value  = slideEvt.value;
			slider.closest('.range-slider-wrap').find('.slider-val').val(value);
			//getRangeSliderValue (slider, slideEvt.value);
		});

		// input range slider 
		$('.slider-val').change(function(){
			var sliderVal = $(this).val();
			$(this).closest('.range-slider-wrap').find('.range-slider').slider('setValue', sliderVal);
			$(this).blur();
		})

		// affix effect for popup buttons block
		$('.contact-dealer-wrap').affix({
      offset: {
        top: $("header").outerHeight(true) + $(".car-photos").outerHeight(true)
      }
		});

	});

	// Contact dealer/Test drive top position on scroll
	
	$(window).scroll(function() {
		var scroll         = $(window).scrollTop(),
				windowHeight   = $(window).innerHeight(),
				popupBtn       = $('.visible-xs .popup-btn, .visible-xs .btn-buy-online'),
				popupBtnHeight = 0,
				popupBtnTop    = 0;

		popupBtn.each(function() {
			popupBtnHeight = $(this).innerHeight();
			popupBtnTop    = $(this).offset().top;

			var scrollSum = windowHeight - popupBtnHeight - popupBtnTop;

			if(scroll >= -(scrollSum)) {
				$(this).find('.popup-btn-icon').addClass('animation');
			}
		});				
	});

	// leae only unique elements in array
	function unique(arr) {
	  var obj = {};
	  for (var i = 0; i < arr.length; i++) {
	    var str = arr[i];
	    obj[str] = true; 
	  }
	  return Object.keys(obj);
	}

})(jQuery);
