(function($){

	//Create Makes and Models lists
	if($('#find-makes').length != 0) populateMakes("find-makes", "find-models");

	$(document).ready(function () {	
		$('.body-styles input, .price-range input').click(function(){
			if($(this).closest('li, div').hasClass('all-wrap')) {
				$(this).closest('.body-styles, .price-range').find('input').prop( "checked", false );
				$(this).prop( "checked", true );
			} else {
				$(this).closest('.body-styles, .price-range').find('.all-wrap input').prop( "checked", false );
			}	
		});

		$('.body-styles ul li, .price-range li').hover(function() {
			$(this).closest('.body-styles, .price-range').find('.all-wrap .checkbox-content').animate({
				opacity: 0.75
			}, 0);
		}, function(){
			$(this).closest('.body-styles, .price-range').find('.all-wrap .checkbox-content').animate({
				opacity: 1
			}, 0);
		});

		// Range sliders 
		$('.range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});
	}); 

	// get range slider value 
	function getRangeSliderValue (container, minVal, maxVal) {
		var min = minVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
			  max = maxVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

		container.closest('.range-item').find('.min').text(minVal);
		container.closest('.range-item').find('.max').text(maxVal);
	}

})(jQuery);