(function($){

	$(document).ready(function () {
		var videoThumb    = $('.video-thumbs a'),
			videoIframe   = $('.main-video iframe');

		// video change on click
		/*videoThumb.click(function (e){
			e.preventDefault();
			var dataHref      = $(this).attr('data-href'),
				hrefAttr   	  = $(this).attr('href'),
				contentTitle = $(hrefAttr).find('h3').html(),
				mobileTitle   = $('#mobile-title');

			videoIframe.attr('src', dataHref);
			mobileTitle.html(contentTitle);
			$('.video-thumbs .active').removeClass('active');
			$(this).parent().addClass('active');
			$('.video-desc').hide(0);
			$(hrefAttr).show(0);
		});*/

    $('.help-banner .close').click(function(){
      $(this).parent().fadeOut();
    }); 

    $('.banner-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      fade: true
    });

		// featured sliders
		$('#featured-slider').slick({
			slidesToShow: 3,
		    slidesToScroll: 3,
		    responsive: [
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
			{
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
			  }
			}]
		});

		$('#dealer-slider').slick({
			slidesToShow: 1,
		    slidesToScroll: 1,
		    dots: true,
			appendDots:$('.dealer-pagination')
		});

		$( ".dealer-pagination" ).prepend('<button class="pag-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>');
		$( ".dealer-pagination" ).append('<button class="pag-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>');

		$(".pag-prev").on('click', function() {
			$('#dealer-slider').slick("slickPrev");
		});
		$(".pag-next").on('click', function() {
			$('#dealer-slider').slick("slickNext");
		});

		$('#adv-models').change(function() {
    	var selectedVal  = $(this).val(),
    		  parent       = $(this).closest('form'),
    		  modelTooltip = parent.find('.advanced-options').find('.select-tooltip');
    	
    	if(selectedVal == '-1') {
    		$('.body-styles').removeClass('disabled');
    		modelTooltip.css({'z-index':'-1'});
    	} else {
    		modelTooltip.css({'z-index':'3'});
    		$('.body-styles').addClass('disabled');
    		$('.body-styles input').prop('checked', false);
    		$('.body-styles h6').html('All Body Styles <span class="caret pull-right"></span>');
    	}
    });

    $('.select-tooltip').click(function(){
    	if($(this).parent().hasClass('body-styles-wrap')) {
				$('.body-styles .select-tooltip').addClass('opened');
    	} else {
	    	$(this).addClass('opened');
    	}
    	
    })
    $('.top-section').click(function(e){
    	var el = e.target;
			if(!($(el).hasClass('select-tooltip'))) {
				$('.opened').removeClass('opened');
			}
    })

		var count = 0;
		changeTitle ($('.price-range'), count);
		changeTitle ($('.body-styles'), count);

		//responsive initialization
		if($(window).width() < 768) {
			initSlider();
		}
	});

	// window resize functions
	$( window ).resize(function() {
		if($(window).width() < 768) {
			initSlider();
		} else {
			unInitSlider();
		}
	});

	function changeTitle (parent, count) {
		parent.find('input').click(function(){
			($(this).is(':checked')) ? count ++ : count --;
			if(count == '0') {
				$(this).closest('.price-range').find('h6').html('All Prices <span class="caret pull-right"></span>');
				$(this).closest('.body-styles').find('h6').html('All Body Styles <span class="caret pull-right"></span>');
  		}	else {
  			$(this).closest('.price-range').find('h6').html('Selected Prices <span class="caret pull-right"></span>');
  			$(this).closest('.body-styles').find('h6').html('Selected Body Styles <span class="caret pull-right"></span>');
  		}
		})
	}

	function initSlider(){
		$('.car-logos').slick({
			slidesToShow: 1,
	    	slidesToScroll: 1,
	    	infinite: false,
	    	dots: true
		});
	}
	function unInitSlider () {
		var target = $('.car-logos');
		if(target.hasClass('slick-initialized'))
		  target.slick("unslick");
	}

		// JS for collapse search sections
	$('.body-styles, .price-range').on('click touchstart', function () {
			 $($(this).data('target')).collapse('toggle');
	});

	// JS for adding custom class for new search | increase padding top
	$('.body-styles .advanced-options-head').on('click', function () {
		$('.top-section-new').toggleClass('collpase-padding');
	});

})(jQuery);
