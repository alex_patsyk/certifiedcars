(function($){

	$(document).ready(function () {	
		$('.to-top').click(function(e) {
			e.preventDefault();
	     	$('html, body').animate({
		        scrollTop: $( $('#wrap') ).offset().top
		    }, 500);
		    return false;
	    });
	    $('#selectMaker').change(function(){
	    	var blockScrollTo = $(this).val();
	    	$('h2 > a').each(function(){
	    		var name = $(this).attr('name');
	    		if(name == blockScrollTo) {
	    			$('html, body').animate({
				        scrollTop: $( this ).offset().top
				    }, 500);
				    return false;
	    		}
	    	});
	    	
	    });
	});

})(jQuery);