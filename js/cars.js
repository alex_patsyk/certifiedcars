
var makes_arr = new Array("Audi", "BMW", "Fiat", "Toyota", "Tesla");

// States
var s_a = new Array();
s_a[0]="All Models";
s_a[1]="A1|A2|A4|A5";
s_a[2]="B1|B2|D3|D4";
s_a[3]="X6|X7|X8";
s_a[4]="T6|T7|T8";
s_a[5]="XTe6|TE7|Te8";

function populateModels( makeElementId, modelElementId ){
	
	var selectedMakeIndex = document.getElementById( makeElementId ).selectedIndex;

	var modelElement = document.getElementById( modelElementId );
	
	modelElement.length=0;	// Fixed by Julian Woods
	modelElement.options[0] = new Option('All Models','-1');
	modelElement.selectedIndex = 0;
	
	var models_arr = s_a[selectedMakeIndex].split("|");
	
	for (var i=0; i<models_arr.length; i++) {
		modelElement.options[modelElement.length] = new Option(models_arr[i],models_arr[i]);
	}
}

function populateMakes(makeElementId, modelElementId){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var makeElement = document.getElementById(makeElementId);
	makeElement.length=  0;
	makeElement.options[0] = new Option('All Makes','-1');
	makeElement.selectedIndex = 0;
	for (var i=0; i<makes_arr.length; i++) {
		makeElement.options[makeElement.length] = new Option(makes_arr[i],makes_arr[i]);
	}

	// Assigned all countries. Now assign event listener for the states.
	if( modelElementId ){
		makeElement.onchange = function(){
			populateModels( makeElementId, modelElementId );
		};
	}
}



function populateModelsAdmin( makeElementId, modelElementId ){
	
	var selectedMakeIndex = document.getElementById( makeElementId ).selectedIndex;

	var modelElement = document.getElementById( modelElementId );
	
	modelElement.length=0;	// Fixed by Julian Woods
	modelElement.options[0] = new Option('Select Model','-1');
	modelElement.selectedIndex = 0;
	
	var models_arr = s_a[selectedMakeIndex].split("|");
	
	for (var i=0; i<models_arr.length; i++) {
		modelElement.options[modelElement.length] = new Option(models_arr[i],models_arr[i]);
	}
}
function populateMakesAdmin(makeElementId, modelElementId){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var makeElement = document.getElementById(makeElementId);
	makeElement.length=  0;
	makeElement.options[0] = new Option('Select Make','-1');
	makeElement.selectedIndex = 0;
	for (var i=0; i<makes_arr.length; i++) {
		makeElement.options[makeElement.length] = new Option(makes_arr[i],makes_arr[i]);
	}

	// Assigned all countries. Now assign event listener for the states.
	if( modelElementId ){
		makeElement.onchange = function(){
			populateModelsAdmin( makeElementId, modelElementId );
		};
	}
}