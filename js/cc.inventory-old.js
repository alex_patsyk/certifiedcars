(function($){
	$(window).on('load', function () {	    
	    equalheight('.view-grid > div');
	});

	$(document).ready(function(){
		var savedBtn      = $('.saved-btn'),
			likeBtn       = $('.like-btn'),
			viewPhoneBtn  = $('.view-phone'),
			resultItem    = $('.result-wrap .result-item, .inventory-cpo-program');
			/*makesArr      = [],
			modelsArr     = [],
			conditionsArr = [],
			bodyArr       = [],
			yearsArr      = [],
			colorsArr     = [],
			/*makesBtn      = $('.makes-filter .checkbox-wrap input'),
			modelsBtn     = $('.models-filter .checkbox-wrap input'),
			conditionsBtn = $('.conditions-filter .checkbox-wrap input'),
			bodyBtn       = $('.body-filter .checkbox-wrap input'),
			colorBtn      = $('.colored-filter-row .color input'),
			yearsBtn      = $('.years-filter .checkbox-wrap input'); */

		// Range sliders 
		$('.range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});

		/*showSelectedCheck (makesArr, makesBtn);
		showSelectedCheck (modelsArr, modelsBtn);
		showSelectedCheck (conditionsArr, conditionsBtn);
		showSelectedCheck (bodyArr, bodyBtn);
		showSelectedCheck (colorsArr, colorBtn);
		showSelectedCheck (yearsArr, yearsBtn);*/

		// clicable itme on inventory pages
		resultItem.click(function(event){
			var that = event.target,
				  thatParent = event.target.parentElement;
			if($(thatParent).hasClass('save-wrap')) {
				event.preventDefault();
				$(this).closest('.result-item').toggleClass('saved');
			}
			if($(that).hasClass('like-btn')) {
				event.preventDefault();
				$(this).closest('.result-item').toggleClass('liked');
			}
			
		});

		savedBtn.click(function(e) {
			e.preventDefault();
			$(this).find('em').toggle();
		});

		$('.result-item .like').click(function(e) {
			e.preventDefault();
			$(this).closest('.result-item').toggleClass('liked');
		})

		// mobile: show/hide filters content 
		/*showFilterBody($('.original-filters .responsive-title'));
		showFilterBody($('.clone-filters .responsive-title'));*/

		/*if($(window).width() < 992) {
			stickyFilters();
		}*/
	});

	$( window ).resize(function() {
		/*if($(window).width() < 991) {
			stickyFilters();
		}*/
		equalheight('.view-grid > div');
		
	});

	// equal height on inventory grid page
	function equalheight (container){
		var currentTallest = 0,
		    currentRowStart = 0,
		    rowDivs = new Array(),
		    $el,
		    topPosition = 0;
		 $(container).each(function() {
			$el = $(this);
			$($el).height('auto')
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		 });
		 addComa($('.coma-format'));
	};

	function addComa (arg) {
		arg.each(function () {
			$(this).text($(this).text().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		})
	}

	// get range slider value 
	function getRangeSliderValue (container, minVal, maxVal) {
		var min = minVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
			max = maxVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

		if(container.hasClass('money-range-slider')) {
			container.closest('.tab-pane, .filter-item-body').find('.min').text(min);
			container.closest('.tab-pane, .filter-item-body').find('.max').text(max);
			container.closest('.filter-item').find('.responsive-title span').text('$' + min + ' - $' + max);

		} else {
			container.closest('.filter-item').find('.min').text(minVal);
			container.closest('.filter-item').find('.max').text(maxVal);
			container.closest('.filter-item').find('.responsive-title span').text(minVal + ' - ' + maxVal + 'M');
		}
	}


	if ($('#back-top').length) {
    var scrollTrigger = 100, 
	      backToTop = function () {
          var scrollTop = $(window).scrollTop();
          if (scrollTop > scrollTrigger) {
              $('#back-top').fadeIn(300);
          } else {
              $('#back-top').fadeOut(300);
          }
      };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}

	// show selected items in filters block on mobile
	/*function showSelectedCheck (arr, btn) {
		btn.click(function(){
			var inputVal = $(this).val(),
				str = '',
				responsiveTitle  = $(this).closest('.filter-item').find('.responsive-title h5 span');

			if($(this).is(':checked')) {
				arr.push(inputVal);
				str = arr.join(', ');
			} else {
				arr.forEach(function(item,i,arr){						
					if(inputVal == arr[i]) {
						arr.splice(i, 1)
					}
				});
				str = arr.join(", ");
			}
			responsiveTitle.html(str);
			
		})
	}*/

	// show/hide filters content
	/*function showFilterBody (btn) {
		btn.on("click", function (){
			var parent = $(this).parent();
			if(parent.hasClass('filters-opened')){
				parent.removeClass('filters-opened').children('.filters-body, .filter-item-body').slideUp();
			} else {
				parent.addClass('filters-opened').children('.filters-body, .filter-item-body').slideDown();
			}
		});
	}*/

	// sticky filters on mobile
	/*var duplicateFilters = $('.filters-wrap').clone().prependTo('.inner-page .row').removeClass('original-filters').addClass('clone-filters'),
		cloneMakes       = $('.clone-filters #makes').removeAttr('id').attr('id', 'clone-makes').attr('title', 'All Makes'),
		cloneModels      = $('.clone-filters #models').removeAttr('id').attr('id', 'clone-models'),
		cloneFrom        = $('.clone-filters #year-from').removeAttr('id').attr('id', 'clone-year-from'),
		cloneTo          = $('.clone-filters #year-to').removeAttr('id').attr('id', 'clone-year-to'),
		cashClone        = $('.clone-filters #cash').removeAttr('id'),		
		financeClone     = $('.clone-filters #finance').removeAttr('id'),
		leaseClone       = $('.clone-filters #lease').removeAttr('id');




	function stickyFilters () {
		var lastScrollTop = 0,	
			navbarHeight = $('.original-filters').outerHeight() + 250;

		if(cloneMakes.length != 0) populateMakes("clone-makes", "clone-models");

		$(cloneMakes).selectpicker({
        	dropupAuto: false,
        	size: '8'
        });

        //range year selects
		getFromYearValue ($('#clone-year-from'), $('#clone-year-to'));
		getToYearValue ($('#clone-year-from'), $('#clone-year-to')) ;


		$(window).scroll(function(event){
		   var st = $(this).scrollTop();
		   if (st > lastScrollTop){
		   		 $('.clone-filters').removeClass('sticky').addClass('non-sticky');
		   		 $('.clone-filters .filters-opened').removeClass('filters-opened').find('.filters-body, .filter-item-body').slideUp();
		   } else {
		   		if(lastScrollTop > navbarHeight) {
					$('.clone-filters').removeClass('non-sticky').addClass('sticky');
		   		} else {
		   			$('.clone-filters').removeClass('sticky').removeClass('non-sticky');
		   		}	      
		   }
		   lastScrollTop = st;
		});
	}*/
	
})(jQuery);