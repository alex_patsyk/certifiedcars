function myMap() {
	var uluru = {lat: 40.423216, lng: -79.766558};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 4,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  map: map
	});
}

(function($){
	$(window).on('load', function () {
	    $('#loading-wrap').animate({
	    	opacity: 0
	    }, 2000, function() {
		    $( this ).remove();
		});
	});

	$('[rel="tooltip"]').tooltip();

	$(document).ready(function(){

		//mobile menu behavior later
		/*$('.mobile-btn').click(function(){
			$('.dashboard-title .tabs-wrap').toggleClass('opened');
		});*/

		$('.drop-btn').click(function(e){
			e.preventDefault();
			var parent = $(this).closest('.drop-wrap');

			if(parent.hasClass('content-opened')) {
				parent.removeClass('content-opened').find('.drop-content').slideUp(500);
				$(this).find('.icon').html('+');
			} else {
				parent.addClass('content-opened').find('.drop-content').slideDown(500);
				$(this).find('.icon').html('&ndash;');
			}		
		});

		$('.settings-tabs .stock-tab').click(function(){
			$('#stock').find('.active').removeClass('active');
			$('#stock #view, #stock a[aria-controls="view"]').addClass('active');
		});

		// show images in input[type='upload']
		$("#file-image").change(function(){
		    readURL(this);
		}); 
		$("#file-image-preview").on('click', function(e){
			if ($(e.target).attr('class') == 'close') {
				$(e.target).parent().remove();	
			}
		});

		// animation for numbers 
		$('.count, .count-time span').counterUp({
            delay: 10,
            time: 1000
        });

        $('.result-container .edit-btn').click(function(e){
        	e.preventDefault();
        	$(this).closest('.tab-pane').removeClass('active');
        	$(this).closest('.tab-content').find('#edit').addClass('active');
        })

        // Progressbar
		if ($(".progress .progress-bar")[0]) {
		    $('.progress .progress-bar').progressbar();
		}

		/* EASYPIECHART */
		 $('.chart').easyPieChart({
		 	animate: 1500,
            barColor:'#999999',
            trackColor: '#167609',
            lineWidth:25,
            lineCap:'circle',
            size: 150,
		 	onStep: function(from, to, percent) {
			$(this.el).find('.percent').text(Math.round(percent));
		  }
		 });


		// Show Settings form on click
		$('.settings-form-btn').click(function(e){
			e.preventDefault();
			$(this).closest('.settings-detail').hide();
			$('.settings-form-wrap').show();
		});

		// Show View on click
		$('.leads-view-btn').click(function(e){
			e.preventDefault();
			$(this).closest('.leads-content-wrap').hide();
			$('.leads-content-view').show();
		});	

		// Reinitialization map
		$('.dashboard-title li').click(function(){
			var windowWidth = $(window).width();
			$(this).closest('.dashboard-title').find('.active').removeClass('active');
			//mobile: hide nav menu on click later
			/*if(windowWidth < 768) {
				$('.dashboard-title .tabs-wrap').removeClass('opened');
			}*/
			google.maps.event.trigger(myMap(), "resize");
	    });

	    $('.nav-tabs a').click(function(e){
	    	e.preventDefault();
			$('.leads-content-wrap').show();
			$('.leads-content-view').hide();
	    })

		// Add Busines hours to Seetins form 
		var count = 0,
			workDayItem = '<div class="hour-item">'+
								'<select name="day'+count+'" class="form-control">'+
									'<option>Select day</option>'+
									'<option>Friday</option>'+
									'<option>Saturday</option>'+
									'<option>Sunday</option>'+
									'<option>Monday</option>'+
									'<option>Tuesday</option>'+
									'<option>Wendesday</option>'+
									'<option>Thursday</option>'+
								'</select>'+
								'<select name="from'+count+'" class="form-control">'+
									'<option>Select hour</option>'+
									'<option>7:00AM</option>'+
									'<option>7:30AM</option>'+
									'<option>8:00AM</option>'+
									'<option>8:30AM</option>'+
									'<option>9:00AM</option>'+
									'<option>9:30AM</option>'+
									'<option>10:00AM</option>'+
								'</select>'+
								'<select name="to'+count+'" class="form-control">'+
									'<option>Select hour</option>'+
									'<option>4:00PM</option>'+
									'<option>4:30PM</option>'+
									'<option>5:00PM</option>'+
									'<option>5:30PM</option>'+
									'<option>6:00PM</option>'+
									'<option>6:30PM</option>'+
									'<option>7:00PM</option>'+
								'</select>'+
							'</div>';

		$('.add-day-btn').click(function(){
			count++;
			if(count < 8) {
				$(this).parent().prepend(workDayItem);
			} else {
				$(this).addClass('disabled');
			}
		});

		// Add/Delete row to Leads view detail table
		$("#add").click(function(){
			var html = ' <tr role="row" class="odd emailRow"><td><span>To: </span><div class="form-group"><input type="text" class="form-control email" placeholder="Email"></div></td><td><span>Subject: </span><div class="form-group"><input type="text" class="form-control subject" placeholder="Subject"></div></td><td><span>Delivered as: </span><select class="form-control format"><option value="adf">ADF/XML</option><option value="text">TEXT</option>option value="simple">Simple Text</option>option value="simpleAdf">Simple ADF/XML</option></select></td><td><span>Lead Source: </span><select class="form-control source"><option value="leads@email.fzautomotive.com">FZAUTOMOTIVE</option><option value="customer">CUSTOMER</option></select></td><td><span>Hide Sensitive:</span><div class="form-group"><input type="checkbox" class="form-control hide-sensitive"></div></td><td class="close-td"> <a class="remove" href="javascript:void(null)"><span class="close-btns"><i class="fa fa-times"></i></span></a></td</tr>';
			$('.rowBody').append(html);
		});
		$(document).on("click",".remove",function(){
			$(this).parent().parent().remove();
		});
		$("#addCondition").click(function(){
			var html = '<tr class="odd rowCond" role="row"><td><select class="form-control firstSel"><option value="Lead Type">Lead Type</option><option value="Category">Category</option><option value="Type">Type</option><option value="Year">Year</option><option>Make</option><option>Model</option></select></td><td><select class="form-control secondSel"><option value="0">Does Not Equal</option> <option value="1">Equals</option><option value="2">Less Than</option><option value="3">Greater Than</option><option value="4">Starts With</option><option value="5">Ends With</option><option value="6">Exists and is not empty</option></select></td><td class="thirdTd"><input class="form-control thirdSel"></td><td class="close-td"><a class="remove" href="javascript:void(null)"><span class="close-btns"><i class="fa fa-times"></i></span></a></td></tr>';
			$("#rowCondition").append(html);
		});

		// Datepickers
		$( "#from" ).datepicker({	  
		  changeMonth: true,
		  numberOfMonths: 1,
		  dateFormat: 'yy-mm-dd',
		  onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
			$("#date-range").val($("#from").val()  + " - " +  $("#to").val());
			 var thistable = $('#dt_basic').dataTable();
		  	//thistable.draw();
		  	otable.draw();
			mytable.fnDraw();
		  }
		});
		$( "#to" ).datepicker({  
		  changeMonth: true,
		  numberOfMonths: 1,
		  dateFormat: 'yy-mm-dd',
		  onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			$("#date-range").val($("#from").val() + " - " + $("#to").val());
			var thistable = $('#dt_basic').dataTable();
			//thistable.draw();
			otable.draw();
			mytable.fnDraw();
		  }
		});

		// Range sliders 
		$('.filters-wrap .range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});

		$('.calculator-item .range-slider').slider({
			formatter: function(value) {
				return '$' + value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			}
		});

		//Create Makes and Models lists
		if($('#makes').length != 0) populateMakesAdmin("makes", "models");

		// mobile: show/hide filters content 
		showFilterBody($('.filters-wrap .responsive-title'));

		$(window).resize(function(){
			drawChart()
		})

	});

	// get range slider value 
	function getRangeSliderValue (container, minVal, maxVal) {
		var min = minVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
			max = maxVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

		if(container.hasClass('money-range-slider')) {
			container.closest('.tab-pane, .filter-item-body').find('.min').text(min);
			container.closest('.tab-pane, .filter-item-body').find('.max').text(max);
			container.closest('.filter-item').find('.responsive-title span').text('$' + min + ' - $' + max);

		} else {
			container.closest('.filter-item').find('.min').text(minVal);
			container.closest('.filter-item').find('.max').text(maxVal);
			container.closest('.filter-item').find('.responsive-title span').text(minVal + ' - ' + maxVal + 'M');
		}
	}

	// show/hide filters content
	function showFilterBody (btn) {
		btn.on("click", function (){
			var parent = $(this).parent();
			if(parent.hasClass('filters-opened')){
				parent.removeClass('filters-opened').children('.filters-body, .filter-item-body').slideUp();
			} else {
				parent.addClass('filters-opened').children('.filters-body, .filter-item-body').slideDown();
			}
		});
	}

	// preview image for input[type='file']  <em class="close">&#215;</em>
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();       
            reader.onload = function (e) {
                $('#file-image-preview').append('<span class="img-item"><img src="' + e.target.result + '" alt=""/></span>');
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

	// Charts 

	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() {
		var piechart_data = new google.visualization.DataTable();
        piechart_data.addColumn('string', 'Topping');
        piechart_data.addColumn('number', 'Slices');
        piechart_data.addRows([
          ['Desktop', 40],
          ['Tablet', 30],
          ['Phone', 30]
        ]);

		var piechart_options = {
		  title: 'Devices',
		  is3D: true,
		  fontSize: 12,
		  backgroundColor: '#f9fafb',
		  colors: ['#e74c3c', '#3498db', '#2ecc71', '#f3b49f', '#f6c7b6']
		};

		var piechart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
		piechart.draw(piechart_data, piechart_options);


		var mobile_data = new google.visualization.DataTable();
        mobile_data.addColumn('string', 'Topping');
        mobile_data.addColumn('number', 'Slices');
        mobile_data.addRows([
          ['Android', 0],
          ['iOS', 0],
          ['Other: 100', 100]
        ]);

		var mobileUsageChart_options = {
			title:'Mobile OS (Mobile & Tablet)',
			fontSize: 12,
			colors: ['#167609'],
            legend: 'none'
        };
        var mobileUsageChart = new google.visualization.BarChart(document.getElementById('MobileUsageBarChart'));
        mobileUsageChart.draw(mobile_data, mobileUsageChart_options);


        var browser_data = new google.visualization.DataTable();
        browser_data.addColumn('string', 'Topping');
        browser_data.addColumn('number', 'Visits');
        browser_data.addRows([
			['IE', 0],
			['Chrome', 0],
			['FireFox', 0],
			['Safari', 0],
			['Other: 100', 100]
        ]);

        var BrowserUsageChart_options = {
			title:'Desktop OS',
			fontSize: 12,
			colors: ['#167609'],
			chartArea: { left: 67, top: 30, width: '60%' }
        };
        var BrowserUsageChart = new google.visualization.BarChart(document.getElementById('BrowserUsageBarChart'));
        BrowserUsageChart.draw(browser_data, BrowserUsageChart_options);
	}

})(jQuery);
