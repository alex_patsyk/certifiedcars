(function($){

	$(document).ready(function () {	
		$('.plan-item').click(function(e) {
			e.preventDefault();
	     	$('#dealer-name').focus();
	     	$('html, body').animate({
		        scrollTop: $( $('.contact-form-wrap') ).offset().top
		    }, 500);
		    return false;
	    })
	});

})(jQuery);