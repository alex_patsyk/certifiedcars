(function($){

	$(document).ready(function(){
		var changeFormBtn = $('.change-form-btn');
		changeFormBtn.click(function(e){
			e.preventDefault();
			var hrefAttr = $(this).attr('href');
			$(this).closest('.authentication-form-wrap').hide();
			$(hrefAttr).show();
		});
	});

})(jQuery);