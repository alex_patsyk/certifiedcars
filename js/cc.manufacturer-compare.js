(function($){

	$(document).ready(function(){
		//responsive initialization
		if($(window).width() < 768) {
			initSlider();
		}
	})

    // window resize functions
	$( window ).resize(function() {
		if($(window).width() < 768) {
			initSlider();
		} else {
			unInitSlider();
		}		
	});

	function initSlider(){
		$('.car-logos').slick({
			slidesToShow: 1,
	    	slidesToScroll: 1,
	    	infinite: false,
	    	dots: true
		});
	}
	function unInitSlider () {
		var target = $('.car-logos');
		if(target.hasClass('slick-initialized'))
		  target.slick("unslick");
	}

})(jQuery);