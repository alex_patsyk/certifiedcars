(function($){

	$(document).ready(function(){
		var newsItem = $('.news-wrap .news-item');
		newsItem.click(function(){
			location.href = $(this).attr('data-href');
		});
		
	});

})(jQuery);