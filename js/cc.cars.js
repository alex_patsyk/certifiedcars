(function($){
	$(window).on('load', function () {	    
	    equalheight('.view-grid > div');
	});

	$(document).ready(function(){
		var likeBtn      = $('.result-item .like'),
				filtersBtn   = $('.popup-btn'),
				filtersClose = $('.search-close'),
				resultItem   = $('.result-item');

		// Range sliders 
		$('.range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value[0], slideEvt.value[1]);
		});

		filtersBtn.click(function(e) {
			e.preventDefault();
			var hrefVal = $(this).attr('href');
			$(hrefVal).show();
		});

		filtersClose.click(function() {
			$(this).closest('#search, #filters').hide();
		});

		resultItem.click(function(event){
			var that 			 = event.target,
				  thatParent = event.target.parentElement,
				  dataHref   = $(this).attr('data-href'); 
			if($(thatParent).hasClass('like')) {
				$(this).closest('.result-item').toggleClass('liked');
			} else if (!($(that).attr('href'))) {
				window.location.href = dataHref; 
			}
		});

		$('body').click(function(e) {
			if( $(e.target).hasClass('finance-icon') ) {
				if($(e.target).hasClass('tooltip-opened')) {
					$('.finance-tooltip').fadeOut();
					$(e.target).removeClass('tooltip-opened');
				} else {
					$('.finance-tooltip').html($(e.target).attr('data-title')).fadeIn();
					$(e.target).addClass('tooltip-opened');
				}
			} else {
				if( $(e.target).hasClass('finance-tooltip') ) {
					return false;
				} else {
					$('.finance-tooltip').fadeOut();
					$('.finance-icon').removeClass('tooltip-opened');
				}
			}
		});


		$('.dealer-affix').affix({
      offset: {
        top: $('header').outerHeight(true) + $('.inventory-top-dealer').outerHeight(true) + 50
      }
		});
		

	});

	$( window ).resize(function() {
		equalheight('.view-grid > div');	
	});

	// equal height on inventory grid page
	function equalheight (container){
		var currentTallest = 0,
		    currentRowStart = 0,
		    rowDivs = new Array(),
		    $el,
		    topPostion = 0;
		 $(container).each(function() {
			$el = $(this);
			$($el).height('auto')
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		 });
	};

	// get range slider value 
	function getRangeSliderValue (container, minVal, maxVal) {
		var min = minVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
			max = maxVal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

		if(container.hasClass('money-range-slider')) {
			container.closest('.tab-pane, .filter-item-body').find('.min').text(min);
			container.closest('.tab-pane, .filter-item-body').find('.max').text(max);
			container.closest('.filter-item').find('.responsive-title span').text('$' + min + ' - $' + max);

		} else {
			container.closest('.filter-item').find('.min').text(minVal);
			container.closest('.filter-item').find('.max').text(maxVal);
			container.closest('.filter-item').find('.responsive-title span').text(minVal + ' - ' + maxVal + 'M');
		}
	}


	if ($('#back-top').length) {
    var scrollTrigger = 100, 
	      backToTop = function () {
          var scrollTop = $(window).scrollTop();
          if (scrollTop > scrollTrigger) {
              $('#back-top').fadeIn(300);
          } else {
              $('#back-top').fadeOut(300);
          }
      };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
})(jQuery);