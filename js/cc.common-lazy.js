(function($){
	$(document).ready(function () {
		var	mobileBtn     = $('.mobile-btn'),
			menuOver      = $('.mobile-menu-over'),
			wrapper       = $('#wrap'),
			dropWrap      = $('.drop-wrap'),
			dropContent   = $('.drop-content'),
			dropBtn 	  = $('.drop-btn'),
			viewPhoneBtn  = $('.view-phone'),
			helpfullBnt   = $('.helpful');

		//Create Makes and Models lists
		if($('#makes').length != 0) populateMakes("makes", "models");
        if($('#adv-makes').length != 0) populateMakes("adv-makes", "adv-models");


        $('.original-filters .makes-select, .single-page .makes-select').selectpicker({
        	dropupAuto: false,
        	size: '8'
        });

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		    $('.selectpicker, .filters-wrap .makes-select').selectpicker('mobile');
		};

		// search select
        $('.makes-select').change(function() {
        	var selectedVal = $(this).val(),
        		parent      = $(this).closest('.form-elements-wrap, .filters-body'),
        		modelsSelect = parent.find('select.models-select'),
        		modelBtn    = parent.find('.models-select .btn');
        	
        	if(selectedVal == '-1') {
        		modelBtn.addClass('disabled');
        		modelsSelect.attr('disabled','disabled');
        	} else {
        		modelBtn.removeClass('disabled');
        		modelsSelect.removeAttr('disabled');
        	}
        	modelsSelect.selectpicker('refresh');
        });

        // mobile menu
		mobileBtn.click(function (){
			wrapper.addClass('menu-opened');
		});
		menuOver.click(function () {
			wrapper.removeClass('menu-opened');
		});

		//show phone number 
		viewPhoneBtn.click(function() {
			$(this).addClass('link-active');
		});

		//toggle class heplfullbutton 
		helpfullBnt.click(function(e){
			e.preventDefault();
			$(this).toggleClass('clicked');
		})

		// custop dropdown content 
		dropBtn.click(function(e){
			e.preventDefault();
			var parent = $(this).closest(dropWrap),
				windowWidth = $(window).width(),
				result = ($(this).closest('div').hasClass('menus-wrap')) ? true : false;

			if(parent.hasClass('content-opened')) {
				if(result) {
					if(windowWidth < 768) {
						parent.removeClass('content-opened').find(dropContent).slideUp(500);
					} else {
						parent.removeClass('content-opened').find(dropContent).hide();
					}
				} else {
					parent.removeClass('content-opened').find(dropContent).slideUp(500);
				}
				if($(this).hasClass('change-text')) $(this).html('Show all');
				if($(this).hasClass('read-more')) $(this).html('Read more');
				if($(this).hasClass('saved-search-drop-btn')) $(this).html('<i class="fa fa-angle-right" aria-hidden="true"></i> View all saved searches');
				if($(this).hasClass('business-hours')) $(this).html('Show business hours <i class="fa fa-angle-down" aria-hidden="true"></i>');
			} else {
				if(result) {
					if(windowWidth < 768) {
						parent.addClass('content-opened').find(dropContent).slideDown(500);
					} else {
						parent.addClass('content-opened').find(dropContent).show();
					}
				} else {
					parent.addClass('content-opened').find(dropContent).slideDown(500);
				}
				if($(this).hasClass('change-text')) $(this).html('Show less');
				if($(this).hasClass('read-more')) $(this).html('Read less');
				if($(this).hasClass('map-btn')) $(this).find('a').html('Hide Map & Directions');
				if($(this).hasClass('saved-search-drop-btn')) $(this).html('<i class="fa fa-angle-right" aria-hidden="true"></i> Show less');
				if($(this).hasClass('business-hours')) $(this).html('Hide business hours <i class="fa fa-angle-down" aria-hidden="true"></i>');
				if($('#map').length !=0 ) {
					google.maps.event.trigger(myMap(), "resize");
				}
			}			
		});

		//range year selects
		getFromYearValue ($('#year-from'), $('#year-to'));
		getToYearValue ($('#year-from'), $('#year-to')) ;

		// scroll down
		$('.scroll-down').on('click', (function(e){
	        e.preventDefault();
	        $('html, body').animate({
		        scrollTop: $( $(this).attr('href') ).offset().top
		    }, 500);
		    return false;
	    }));

	    //responsive initialization
		if($(window).width() < 768) {
			$('header .drop-wrap').addClass('content-opened');
		}

	});

})(jQuery);

// get year value

function getFromYearValue (yearFrom, yearTo) {
	yearFrom.on('change', function(){
		var tes = $('option:selected', this).val(); 

		yearTo.find('option').each(function(){
			var curr = $(this).val();
			if(curr <= tes) {
				$(this).attr('disabled', true);
			} else {
				$(this).attr('disabled', false);
			}
		});	
		$('.selectpicker').selectpicker('refresh');
	});
}

function getToYearValue (yearFrom, yearTo) {
	yearTo.on('change', function(key){
		var tes = $('option:selected', this).val();
		yearFrom.find('option').each(function(key){
			var curr = $(this).val();
			if(curr >= tes) {
				$(this).attr('disabled', true);
			} else {
				$(this).attr('disabled', false);
			}
		});
		$('.selectpicker').selectpicker('refresh');	
	});
}