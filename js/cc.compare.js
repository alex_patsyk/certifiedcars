(function($){

	$(document).ready(function () {	
		stickySelect();	
	});

	function stickySelect() {
		var tableHeadHeight = $('.table-header').outerHeight(),	
			tableTop = $('.compare-table-wrap').offset().top,
			sum = tableHeadHeight + tableTop;

		$(window).scroll(function(event){
		   var st = $(this).scrollTop();
		   if (st > sum){
		   		 $('.compare-table-wrap').addClass('sticky-header');
		   } else {
				$('.compare-table-wrap').removeClass('sticky-header');      
		   }
		});
	}

})(jQuery);