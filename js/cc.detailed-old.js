function myMap() {
	var uluru = {lat: 40.423216, lng: -79.766558};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 4,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  map: map
	});
}

(function($){

	$(document).ready(function(){
		var readBtn       = $('.read-btn'),
			shareBtn 	      = $('.share-btn'), 
			likeBtn         = $('.like-btn'),
			viewPhoneBtn    = $('.view-phone'),
			networkOver     = $('.mobile-networks-over'),
			networkCloseBtn = $('.mobile-networks .close-btn'),
			navSliderHeight = $('.nav-main-photo-slider').outerHeight(),
			popupContainer  = $('.popups-container');


		// hide slider-nav on resize
		$(window).resize(function(){
			var windowWidth = $(window).width();

			navSliderHeight = $('.nav-main-photo-slider').outerHeight();
			if(windowWidth < 768) {
				$('.nav-main-photo-slider-wrap').css({bottom: -navSliderHeight}).addClass('closed');
			}
		});

		var windowWidth = $(window).width();
		if(windowWidth < 768) {
			$('.nav-main-photo-slider-wrap').css({bottom: -navSliderHeight}).addClass('closed');
		}
 
		$('.slider-action-btn').click(function(){
			var parent = $(this).closest('.nav-main-photo-slider-wrap');
			console.log(navSliderHeight);
			if(parent.hasClass('closed')) {
				parent.removeClass('closed');
				parent.animate({
					bottom: 0
				}, 300);
			} else {
				parent.addClass('closed');
				parent.animate({
					bottom: -navSliderHeight
				}, 300);
			}
		});

		//show/hide popups 
		$(".popup-btn").click(function(e){
			e.preventDefault();
			var hrefItem = $(this).attr('href'),
					windowWidth = $(window).width();

			popupContainer.addClass('visible'); 
			if (windowWidth > 1199) {
				$('body').stop().animate({scrollTop:0}, 300).addClass('overflow');
			}
			if(windowWidth > 767) {
				$('.popup-btns .btn').removeClass('active');
				$(this).addClass('active'); 
				$('.popups-content-item').fadeOut(0);
				$(hrefItem).delay(300).fadeIn();
			} else {
				$(hrefItem).height(window.innerHeight).addClass('visible');
				$('.buy-online-over').fadeIn();
			}	
			if($(this).hasClass('exclusive-price-btn')) {
				$('.popups-wrap .popups-header').hide(0);
			}	else {
				$('.popups-wrap .popups-header').delay(300).fadeIn();
			}
		});
		

		$('.popups-close, .popups-header .car-photo, .popups-header h2, .buy-online-over').click(function(){
			var windowWidth = $(window).width();
			if(windowWidth > 767) {
				$('.popups-content-item, #buy-online, .buy-online-form, .buy-online-over').fadeOut();
				popupContainer.removeClass('visible');
				$('body').removeClass('overflow');
				$('.popup-btns .btn').removeClass('active');
			} else {
				$('.popups-content-item').removeClass('visible');
				popupContainer.animate({
					height: 'auto'
				},500, function(){
					popupContainer.removeClass('visible');
				});
				$('.buy-online-over').fadeOut();
			}
			$('body').removeClass('overflow');
		});

		$('.popup-btns .btn-buy-online').click(function(e){
			e.preventDefault();
			var hrefItem = $(this).attr('href'),
					windowWidth = $(window).width();
			if(windowWidth > 767) {
				$('body').stop().animate({scrollTop:0}, 300).addClass('overflow');
				$(hrefItem).delay(300).fadeIn();
				$('.buy-online-form, .buy-online-over').delay(300).fadeIn();
			}		 
		})

		// affix effect for popup buttons block
		$('#affix, #affix-popup').affix({
      offset: {
        top: 113,
        bottom: $("footer").outerHeight(true) + 20
      }
		});

		// sharing BUTTONS
		$("#desktop-sharing").jsSocials({
		    showCount: false,
		    showLabel: false,
		    shares: ["twitter", "facebook", "googleplus"]
		});
		$("#mobile-sharing").jsSocials({
		    showCount: false,
		    showLabel: true,
		    shares: ["email", "twitter", "facebook", "googleplus", "whatsapp"]
		});

		// photo slider
		$('.main-photo-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			adaptiveHeight: true,
			asNavFor: '.nav-main-photo-slider',
			responsive: [
			    {
			      breakpoint: 767,
			      settings: {
			        arrows: true,
			        fade: false
			      }
			    }
			]
		});
		$('.nav-main-photo-slider').slick({
			slidesToShow: 13,
			slidesToScroll: 1,
			asNavFor: '.main-photo-slider',
			arrows: true,
			focusOnSelect: true,
			speed: 700,
			variableWidth: true,
			autoplay: true,
			autoplaySpeed: 3000,
			centerMode: true,
			responsive: [
        {
          breakpoint: 1200,
          settings: {
              slidesToShow: 10
          }
        },
        {
          breakpoint: 992,
          settings: {
              slidesToShow: 8
          }
        },
        {
          breakpoint: 767,
          settings: {
              slidesToShow: 8
          }
        }
      ]
		});

		setTimeout(function(){
			$('.nav-main-photo-slider').slick('slickPause');
		}, 30000);

		// read full description
		readBtn.click(function(e){
			e.preventDefault();
			var parent = $(this).parent();
			if(parent.hasClass('more')) {
				parent.removeClass('more');
				$(this).html('SHOW MORE <span>+</span>');
				$(this).closest('.vehicle-overview').find('.mobile-hide').hide(500);
			} else {
				parent.addClass('more');
				$(this).html('SHOW LESS <span>-</span>');
				$(this).closest('.vehicle-overview').find('.mobile-hide').show(500);
			}
		});

		likeBtn.click(function(){
			$(this).closest('.request-detail').toggleClass('liked');
		})

		shareBtn.click(function(e) {
			e.preventDefault();
			$('.mobile-networks').addClass('visible');
			$('.mobile-networks-over').addClass('visible');
		});
		networkOver.click(function(){
			$(this).removeClass('visible');
			$('.mobile-networks').removeClass('visible');
		});
		networkCloseBtn.on('click', function(){
			$(this).parent().removeClass('visible');
			$('.mobile-networks-over').removeClass('visible');
		});

		// Add class for colored select 
		$('.popups-content-item select').change(function(){
			if($(this).val() !== '-1') {
				$(this).closest('.select-wrap').addClass('selected');
			} else {
				$(this).closest('.select-wrap').removeClass('selected');
			}
		});

		// Checking on emplty/ show error message
		$('.popups-content-item .btn').click(function(){
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i,
					count = 0,
					countVal = 0;
			$(this).closest('.popups-content-item').find('input.required, select.required').each(function(){
				count ++;
				if($(this).val() == "" || $(this).val() == "-1") {
					$(this).addClass('error');
					$(this).closest('.select-wrap').addClass('error');
				} else {
					if(!(pattern.test($(this).attr('type','email').val()))){
		        $(this).addClass('error');
		        $('.error').selectpicker('refresh');
		      }
		      $(this).removeClass('error');
		      $(this).closest('.select-wrap').removeClass('error');
		      countVal ++;
				}
			});
			if(countVal < count) {
				$('#error-message').show();
				return false;
			}
		});

		$('.popups-content-item').on('change', '.error', function(){
			$(this).closest('.select-wrap').removeClass('error');
			$(this).removeClass('error');
			$('#error-message').hide();
		});

		// Disable forms page refresh
		/*$('#finance-form').on('keyup keypress', "input", function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) {
		    e.preventDefault();
		    return false;
		  }
		});*/

		/* add class fof colored bg
		$('.test-drive-form-wrap').click(function (e) {
			var that = e.target;
			if($(that).hasClass('filter-option')) {
				$('.test-drive-form-wrap').addClass('select-opened ');
			} else {
				$('.test-drive-form-wrap').removeClass('select-opened ');
			}
		});*/
		// remove disabled station
		/*$('.test-drive-form-wrap form').change(function(){
			var selectCount = 0;
			$(this).find('.input').each(function(){
				if($(this).val() !== '') {
					selectCount++;
				} else {
					selectCount--;
				}
			});
			$(this).find('select').each(function(){
				if($(this).val() !== '-1') {
					$(this).closest('.select-wrap').addClass('selected');
					selectCount++;
				} else {
					$(this).closest('.select-wrap').removeClass('selected');
					selectCount--;
				}
			});
			if(selectCount == '5') {
				$(this).find('.btn').addClass('active').prop('disabled', false);
			}
		});*/


		// JS code for Finance range sliders
		$('.range-slider').slider({
			tooltip_split: true,
			tooltip: 'hide'
		});
		$('.range-slider').on('slide', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value);
		});	
		$('.range-slider').on('slideStop', function(slideEvt){
			var slider = $(this)
			getRangeSliderValue (slider, slideEvt.value);
		});

		// JS code fo down payment slider
		/*$('#down-payment-slider').slider();


		$("#down-payment-slider").on("change", function(slideEvt) {
			$("#down-payment-value").val(Number(slideEvt.value.newValue).toLocaleString('en'));
			$("#down-payment-value-2").html(Number(slideEvt.value.newValue).toLocaleString('en'));
		});

		// JS code for credit score rate
		$("#credit-score-slider").on("change", function(slideEvt) {
			var CreditRate = (slideEvt.value.newValue);
			if (CreditRate == 1) {
					$("#credit-rate").text("POOR");
			 }
			 else if (CreditRate == 2) {
					$("#credit-rate").text("FAIR");
			 }
			 else if (CreditRate == 3) {
					$("#credit-rate").text("GOOD");

			 }
			 else if (CreditRate == 4) {
					$("#credit-rate").text("EXCELLENT");

			 }
			 else {
					$("#credit-rate").text("EXCELLENT");
			 }
		});

		// JS code for low interest slider
		$("#low-interest-slider").on("change", function(slideEvt) {
			var lowInterestValue = (slideEvt.value.newValue);

			 if (lowInterestValue == 1) {
					$("#low-interest-value").text("36 months");
					$("#low-interest-value-2").text("36 months");
					$("#apr-value").text("2.49% APR");
			 }
			 else if (lowInterestValue == 2) {
					$("#low-interest-value").text("48 months");
					$("#low-interest-value-2").text("48 months");
					$("#apr-value").text("2.49% APR");
			 }
			 else if (lowInterestValue == 3) {
					$("#low-interest-value").text("60 months");
					$("#low-interest-value-2").text("60 months");
					$("#apr-value").text("2.9% APR");
			 }
			 else if (lowInterestValue == 4) {
					$("#low-interest-value").text("72 months");
					$("#low-interest-value-2").text("72 months");
					$("#apr-value").text("2.9% APR");
			 }
			 else {
					$("#low-interest-value").text("72 months");
					$("#low-interest-value-2").text("72 months");
					$("#apr-value").text("2.9% APR");

			 }
		});

		// JS code for refresh slider on tab click
		$('.finance-content, .finance-btn').one("click touchstart", function() {
			setTimeout(function () {
				$('#credit-score-slider, #low-interest-slider').slider('refresh');
			}, 10);
		});

		// JS code to calculate price per month

		var result;
		var monthyPayment;
		$('.finance-summary-values').bind('DOMNodeInserted DOMNodeRemoved', function() {
			var monthsNumber = $("#low-interest-value-2").text().replace(/[^\d.-]/g, '');
			var downPayment = $("#down-payment-value-2").text().replace(/[^\d.-]/g, '');
			var carPrice = $(".price-detail-value").text().replace(/[^\d.-]/g, '');
			var annualRate = $("#apr-value").text().replace(/[^\d.-]/g, '') / 100 ;

			if ($(downPayment).text().length == 0 ) {
				carPrice = carPrice - downPayment;
			}

			var v1 = carPrice * ( (annualRate / 12) * Math.pow(( 1 + annualRate / 12 ), monthsNumber) );
			var v2 = Math.pow(( 1 + annualRate / 12 ), (monthsNumber) ) - 1;
			result = v1 / v2
			monthyPayment = $("#monthly-payment").text('$' + ((Number(result).toLocaleString('en')))).text(Number(result).toFixed(2));

			// change slider value on input change
			var tradeValue;
			var amountFinanced;
			$("#trade-in-value, #down-payment-value").on("change paste keyup", function() {
				  tradeValue = $(this).val().replace(/[^\d.-]/g, '');;
					amountFinanced =  parseInt(downPayment) + parseInt(tradeValue);
					monthyPayment = $(".amount-finance-value").text('$ ' + ((Number(amountFinanced).toLocaleString('en'))));
			});

		});


		$('.send-msg-btn').on("click", function() {
			$( ".finance-content.content-opened .drop-btn, .question-content.content-opened .drop-btn" ).trigger( "click" ).addClass('checked-tab');
		});

		var taxText = 'incl. taxes and fees, on approved credit';
		$('.get-deal-btn').on("click", function() {
			monthyPayment = $("#monthly-payment").text();
			$('.finance-sub-text').text(( '$' + monthyPayment + '/month'));
			$('.finance-tax-text').text(taxText);
			$( ".finance-content.content-opened .drop-btn, .question-content.content-opened .drop-btn" ).trigger( "click" ).addClass('checked-tab');
		});*/


		// JS code for buy online tab
    /*$('.buy-online-tab, .close-frame, .buy-online-btn').click(function(e){
			e.preventDefault();
	    var hidden = $('.buy-online-content');
	    if (hidden.hasClass('visible')){
	        hidden.animate({"left":"100%"}, "slow").removeClass('visible');
	    } else {
	        hidden.animate({"left":"0"}, "slow").addClass('visible');
	    }
    });*/

		/*if ($(window).width() < 768) {
		   $(".detailed-tabs").appendTo("#res-tabs-container");
		}*/

		$('body').on("click touchstart", ".get-price-btn", function(e){

			$(".close-modal").trigger('click');
 			$(".get-price-wrapper").hide();
		  $(".exclusive-price-wrapper").show();
			$(".exclusive-price-msg").show();


		});


		// change slider value on input change
		/*$("#down-payment-value").on("change paste keyup", function() {
			 $('#down-payment-slider').slider('setValue', $(this).val());
		});

    $("input[type='text']").keyup(function(event){
		      // skip for arrow keys
		      if(event.which >= 37 && event.which <= 40){
		          event.preventDefault();
		      }
		      var $this = $(this);
		      var num = $this.val().replace(/,/gi, "").split("").reverse().join("");

		      var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));
		      $this.val(num2);
		  });

		function RemoveRougeChar(convertString){
		    if(convertString.substring(0,1) == ","){

		        return convertString.substring(1, convertString.length)
		    }
		    return convertString;
		}*/

		//double tap on mobile to open fancybox
	    /*if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		    var gallery = [];
		    $(".fancy").each(function (i) {
		        gallery.push({'href' :this.href });
		    });

		    var tapped=false
			$(".fancy").on("touchstart",function(e){
			    if(!tapped){ //if tap is not set, set up single tap
			      tapped=setTimeout(function(){
			          tapped=null
			          //insert things you want to do when single tapped
			      },300);   //wait 300ms then run single click code
			    } else {    //tapped within 300ms of last tap. double tap
			    	clearTimeout(tapped); //stop single tap callback
			    	tapped=null
			    	//insert things you want to do when double tapped
			      	$.fancybox.open(gallery);
			    }
			    e.preventDefault()
			});
		}*/
	});

	/*(function(d, tag, apikey){

	    var tmv = {

	        init: function() {

	            tmv.loadjs('http://widgets.edmunds.com/js/edm/sdk.js', tmv.loadWidget);

	        },

	        loadjs: function(file, callback) {

	            var fjs = d.getElementsByTagName(tag)[0],

	                s = d.createElement(tag);

	            s.onload = s.onreadystatechange = function() {

	                var r = this.readyState;

	                if (!r || r === 'loaded' || r === 'complete') {

	                    callback.call();

	                    s.onreadystatechange = null;

	                }

	            };

	            s.src = file;

	            fjs.parentNode.insertBefore(s, fjs);

	        },

	        loadWidget: function() {

	            tmv.loadjs('http://widgets.edmunds.com/js/tmv/tmvwidget.js', tmv.initWidget);

	        },

	        initWidget: function() {

	            var widget = new EDM.TMV(apikey, {root: 'tmvwidget', baseClass: 'tmvwidget'});

	            widget.init({"includedMakes":"all","price":"tmv-invoice-msrp","showVehicles":"USED","zip":"10022"});

	            widget.render();

	        }

	    }

	    tmv.init();

	}(document, 'script', 'nyypnzrss7b8nznfk8wkx8ce'));*/

	// get range slider value 
	function getRangeSliderValue (container, value) {
		var value = value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		container.closest('.range-slider-wrap').find('.slider-val').text(value); 
	}

})(jQuery);
